function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function
	const uniq_first_set = new Set(first);
	const uniq_second_set = new Set(second);

	var uniq_first = [...uniq_first_set];
	var uniq_second = [...uniq_second_set];

	if(Array.isArray(first) && Array.isArray(second)){

		var temp = [];
		uniq_first = uniq_first.toString().split(',');
		uniq_second = uniq_second.toString().split(',');

		for (var i in uniq_first) {
			if(uniq_second.indexOf(uniq_first[i]) === -1)
				temp.push(uniq_first[i]);
		}
		for(i in uniq_second) {
			if(uniq_first.indexOf(uniq_second[i]) === -1) 
				temp.push(uniq_second[i]);
		}

		if(uniq_first.length === 0 || uniq_second.length ===0){
			return 0;
		}
		else{
			return temp.sort((a,b) => a-b).length;
		}
	} 
	else{
		throw new Error( 'InvalidType');
	}
}

console.log(distance(['a','b','a'], ['c']));


module.exports.distance = distance